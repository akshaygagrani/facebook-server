# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from . import server
# Create your views here.
from . import server

def text_func(request):
    if request.POST:
        server.run()
        print("Got the POST request")
    return render(request, 'home.html')