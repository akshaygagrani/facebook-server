# -*- coding: UTF-8 -*-
import sys
import pickle
import subprocess
# *- encoding: utf-8 -*-
import sys
import os
#
# print(BASE_DIR)
BASE_DIR = '/home/akshay/Documents/twitter-django2/twitter_app'
options = os.path.join(BASE_DIR,'options.pickle')
with open(options, 'rb') as f:
    options = pickle.load(f)

sys.path.append('%s/python/ner' % (BASE_DIR))
import Features

sys.path.append('%s/hbc/python' % (BASE_DIR))

from LdaFeatures import LdaFeatures

sys.path.append('%s/python/cap' % (BASE_DIR))
import cap_classifier
#
# sys.path.append('%s/python' % (BASE_DIR))
# import pos_tagger_stdin
# import chunk_tagger_stdin
# import event_tagger_stdin
#
#
# from LdaFeatures import LdaFeatures
from Dictionaries import Dictionaries
from Vocab import Vocab
BASE_DIR = '/home/akshay/Documents/twitter_nlp-master'


def GetLLda():
    return subprocess.Popen(
        '%s/hbc/models/LabeledLDA_infer_stdin.out %s/hbc/data/combined.docs.hbc %s/hbc/data/combined.z.hbc 100 100' % (
            BASE_DIR, BASE_DIR, BASE_DIR),
        shell=True,
        close_fds=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE)


if options.classify:
    llda = GetLLda()
else:
    llda = None


def GetNer(ner_model, memory="256m"):
    return subprocess.Popen('java -Xmx%s -cp %s/mallet-2.0.6/lib/mallet-deps.jar:%s/mallet-2.0.6/class cc.mallet.fst.SimpleTaggerStdin --weights sparse --model-file %s/models/ner/%s' % (memory, BASE_DIR, BASE_DIR, BASE_DIR, ner_model),
                           shell=True,
                           close_fds=True,
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE)

def GetLLda():
    return subprocess.Popen('%s/hbc/models/LabeledLDA_infer_stdin.out %s/hbc/data/combined.docs.hbc %s/hbc/data/combined.z.hbc 100 100' % (BASE_DIR, BASE_DIR, BASE_DIR),
                           shell=True,
                           close_fds=True,
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE)

if options.pos and options.chunk:
    ner_model = 'ner.model'
elif options.pos:
    ner_model = 'ner_nochunk.model'
else:
    ner_model = 'ner_nopos_nochunk.model'


dictMap = {}
i = 1
for line in open('%s/hbc/data/dictionaries' % (BASE_DIR)):
    dictionary = line.rstrip('\n')
    dictMap[i] = dictionary
    i += 1

dict2index = {}
for i in dictMap.keys():
    dict2index[dictMap[i]] = i

if llda:
    dictionaries = Dictionaries('%s/data/LabeledLDA_dictionaries3' % (BASE_DIR), dict2index)  # 26.8


entityMap = {}
i = 0
if llda:
    for line in open('%s/hbc/data/entities' % (BASE_DIR)):
        entity = line.rstrip('\n')
        entityMap[entity] = i
        i += 1

dict2label = {}
for line in open('%s/hbc/data/dict-label3' % (BASE_DIR)):
    (dictionary, label) = line.rstrip('\n').split(' ')
    dict2label[dictionary] = label

